﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTopWeakSpot : MonoBehaviour, IHittable {
    public FloatReference maxHP;
    public TestDangly dangly;
    public TestBossBody body;

    private float hp;

    public void Start()
    {
        hp = maxHP.Value;
    }

    public void Hit(float damage, Vector2 facingVector)
    {
        hp -= damage;
        if (hp <= 0)
        {
            dangly.StartAttack();
            body.Damage(1);
            body.StartAdvance();
            hp = maxHP.Value;
        }
    }
}
