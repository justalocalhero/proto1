﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDangly : MonoBehaviour, IHittable {
    public FloatReference upWaitTimeMax;
    public FloatReference upSpeed;
    public FloatReference downWaitTimeMax;
    public FloatReference downSpeed;
    public FloatReference maxHP;

    public Transform upPoint;
    public Transform downPoint;

    public TestBossBody body;

    private Transform target;

    private bool moving;
    private enum Targets { up, down };
    private Targets currentTarget;
    private float waitTimeMax;
    private float waitTime;
    private float speed;
    private float hp;

    void Start()
    {
        moving = false;
        speed = 0;
        waitTime = 0;
        waitTimeMax = 0;
        hp = maxHP.Value;

    }

    void Update()
    {
        if (target != null && moving)
        {
            if (waitTime > 0)
            {
                waitTime -= Time.deltaTime;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
                if (Vector3.Distance(transform.position, target.position) < .1f)
                    moving = false;
            }

        }

    }

    public void Hit(float damage, Vector2 facing)
    {
        hp -= damage;
        if(hp <= 0)
        {
            StartRetreat();
            body.StartRetreat();
        }
    }

    public void StartAttack()
    {
        StartMove(Targets.down);
    }

    public void StartRetreat()
    {
        StartMove(Targets.up);
    }

    private void StartMove(Targets targetting)
    {
        if (!moving)
        {
            currentTarget = targetting;
            speed = GetSpeed(targetting);
            waitTimeMax = GetWaitTime(targetting);
            target = GetTarget(targetting);
            moving = true;
        }
        else
            Debug.Log("Dangly already moving");

    }

    private float GetSpeed(Targets target)
    {
        switch (target)
        {
            case Targets.up:
                return upSpeed.Value;
            case Targets.down:
                return downSpeed.Value;
            default:
                return 0f;
        }
    }

    private float GetWaitTime(Targets target)
    {
        switch (target)
        {
            case Targets.up:
                return upWaitTimeMax.Value;
            case Targets.down:
                return upWaitTimeMax.Value;
            default:
                return 0f;
        }
    }

    private Transform GetTarget(Targets target)
    {
        switch (target)
        {
            case Targets.up:
                return upPoint;
            case Targets.down:
                return downPoint;
            default:
                return null;
        }
    }
}
