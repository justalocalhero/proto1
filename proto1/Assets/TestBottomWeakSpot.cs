﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBottomWeakSpot : MonoBehaviour, IHittable {

    public FloatReference maxHP;
    public TestBossBody body;

    private float hp;

    public void Start()
    {
        hp = maxHP.Value;
    }

    public void Hit(float damage, Vector2 facingVector)
    {
        hp -= damage;
        if(hp <= 0)
        {

        }
    }
}
