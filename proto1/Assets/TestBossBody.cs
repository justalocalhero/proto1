﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBossBody : MonoBehaviour, IHittable {
    public FloatReference forwardWaitTimeMax;
    public FloatReference forwardSpeed;
    public FloatReference backWaitTimeMax;
    public FloatReference backSpeed;
    public FloatReference maxHP;

    public Transform forwardPoint;
    public Transform backPoint;

    private Transform target;    

    private bool moving;
    private enum Targets { forward, back };
    private Targets currentTarget;
    private float waitTimeMax;
    private float waitTime;
    private float speed;
    private float hp;

    public void Start()
    {
        hp = maxHP.Value;
    }

    void Update()
    {
        if (target != null && moving)
        {
            if (waitTime > 0)
            {
                waitTime -= Time.deltaTime;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
                if (Vector3.Distance(transform.position, target.position) < .1f)
                    moving = false;
            }

        }

    }

    public void StartAdvance()
    {
        StartMove(Targets.forward);
    }

    public void StartRetreat()
    {
        StartMove(Targets.back);
    }

    public void Hit(float damage, Vector2 facingVector)
    {

    }

    public void Damage(float damage)
    {
        hp -= damage;
        if(hp <= 0)
        {
            Debug.Log("You Win");
        }
    }

    private void StartMove(Targets targetting)
    {
        if (!moving)
        {
            currentTarget = targetting;
            speed = GetSpeed(targetting);
            waitTimeMax = GetWaitTime(targetting);
            target = GetTarget(targetting);
            moving = true;
        }
        else
            Debug.Log("Dangly already moving");

    }

    private float GetSpeed(Targets target)
    {
        switch (target)
        {
            case Targets.forward:
                return forwardSpeed.Value;
            case Targets.back:
                return backSpeed.Value;
            default:
                return 0f;
        }
    }

    private float GetWaitTime(Targets target)
    {
        switch (target)
        {
            case Targets.forward:
                return forwardWaitTimeMax.Value;
            case Targets.back:
                return forwardWaitTimeMax.Value;
            default:
                return 0f;
        }
    }

    private Transform GetTarget(Targets target)
    {
        switch (target)
        {
            case Targets.forward:
                return forwardPoint;
            case Targets.back:
                return backPoint;
            default:
                return null;
        }
    }
}
