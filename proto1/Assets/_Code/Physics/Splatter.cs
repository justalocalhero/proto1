﻿using UnityEngine;

public class Splatter : MonoBehaviour {
    private Vector2 facingVector;
    public Animator animator;
    public Rigidbody2D rb2d;
    public BoxCollider2D Collider;

    public void Start()
    {
        Destroy(rb2d, 5f);
        Destroy(Collider, 5f);
    }

    public void SetFacing(Vector2 facing)
    {
        facingVector = facing;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "TileColide")
        {
            animator.Play("SplatterFlat");
            rb2d.velocity = Vector2.zero;

        }
    }
}
