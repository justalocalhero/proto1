﻿using UnityEngine;

public class Bullet : MonoBehaviour {
    public FloatReference BulletLifetime;
    public FloatReference BulletSpeed;
    public FloatReference BulletDamage;

    public AudioSource Gunshot;
    public SpriteRenderer rend;
    public Rigidbody2D rb2d;
    public CircleCollider2D Collider;

    private float lifetime;
    private Vector2 facingVector;

    public void FixedUpdate()
    {
        float xSet = facingVector.x * BulletSpeed.Value * Time.deltaTime;
        float ySet = facingVector.y * BulletSpeed.Value * Time.deltaTime;
        float zSet = 0;

        Vector3 toAdd = new Vector3(xSet, ySet, zSet);

        transform.position += toAdd;
        lifetime += Time.deltaTime;

        if(lifetime > BulletLifetime.Value)
        {
            Destroy(this.gameObject);
        }
	}

    public void SetFacing(Vector2 facing)
    {
        facingVector = facing;
    }

    public void ResetLifetime()
    {
        lifetime = 0;
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        bool toDestroy = false;
        bool toDamage = false;

        if(col.gameObject.tag == "Enemy")
        {
            toDestroy = true;
            toDamage = true;
        }

        if(col.gameObject.tag == "TileColide")
        {
            toDestroy = true;
        }

        if (toDamage)
        {
            EnemyController e = col.gameObject.GetComponent<EnemyController>();
            
            if(e != null)
            {
                e.DealDamage(BulletDamage.Value, facingVector);
            }
        }

        if (toDestroy)
        {
            rend.enabled = false;
            Destroy(rb2d);
            Destroy(Collider);
            Destroy(this.gameObject, .5f);
        }
    }
}
