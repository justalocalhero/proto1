﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour
{
    public FloatReference gravity;
    public FloatReference gravityModifier;
    public FloatReference gravityModifierHeavy;
    public FloatReference minGroundNormalY;
    public FloatReference minMoveDistance;
    public FloatReference shellRadius;
    public FloatReference maxFallSpeed;


    protected Vector2 targetVelocity;
    protected bool grounded;
    protected bool heavy;
    protected bool facingRight;
    protected Vector2 facingVector;
    protected Vector2 groundNormal;
    protected Vector2 velocity;
    protected Rigidbody2D rb2d;
    protected Animator animator;
    protected ContactFilter2D contactFilter;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

    void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Start()
    {
        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;

        facingRight = true;
        facingVector = Vector2.right;
        heavy = true;
    }

    void Update()
    {
        targetVelocity = Vector2.zero;
        ComputeVelocity();
    }

    protected virtual void ComputeVelocity()
    {

    }

    void FixedUpdate()
    {
        float gravMod = gravityModifier.Value;
        if (heavy) gravMod *= gravityModifierHeavy.Value;

        velocity += Vector2.down * gravMod * gravity.Value * Time.deltaTime;
  
      
        velocity.x = targetVelocity.x;

        grounded = false;

        Vector2 deltaPosition = velocity * Time.deltaTime;

        Vector2 moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);

        Vector2 move = moveAlongGround * deltaPosition.x;

        Movement(move, false);

        move = Vector2.up * deltaPosition.y;
        move.y = Mathf.Clamp(move.y, -maxFallSpeed.Value, maxFallSpeed.Value);

        Movement(move, true);
    }

    void Movement(Vector2 move, bool yMovement)
    {
        float distance = move.magnitude;

        if (distance > minMoveDistance.Value) 
        {
            int count = rb2d.Cast(move, contactFilter, hitBuffer, distance + shellRadius.Value);
            hitBufferList.Clear();
            for (int i = 0; i < count; i++) {
                hitBufferList.Add(hitBuffer[i]);
            }

            for (int i = 0; i < hitBufferList.Count; i++) 
            {
                Vector2 currentNormal = hitBufferList[i].normal;
                if (currentNormal.y > minGroundNormalY.Value) 
                {
                    grounded = true;
                    if (yMovement)
                    {
                        groundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }

                float projection = Vector2.Dot(velocity, currentNormal);
                if (projection < 0) 
                {
                    velocity = velocity - projection * currentNormal;
                }

                float modifiedDistance = hitBufferList[i].distance - shellRadius.Value;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }


        }

        Vector2 toAdd = move.normalized * distance;
        Vector3 toReallyAdd = new Vector3(toAdd.x, toAdd.y, 0);
        transform.position += toReallyAdd;
        

    }

    public void SetFacing(bool willFaceRight)
    {
        facingRight = willFaceRight;
        facingVector = willFaceRight ? Vector2.right : Vector2.left;
        int yRotation = willFaceRight ? 0 : 180;
        transform.localRotation = Quaternion.Euler(0, yRotation, 0);


    }
}