﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour {

	
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            PlayerController e = collision.gameObject.GetComponent<PlayerController>();
            e.Damage(1, Vector3.left);
        }
    }
}
