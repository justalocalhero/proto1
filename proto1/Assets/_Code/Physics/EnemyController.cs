﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : PhysicsObject {
    public FloatVariable maxHP;
    public FloatReference runSpeed;
    public FloatReference trackRangeMin;
    public FloatReference trackRangeMax;
    public FloatVariable rev;
    public FloatReference minTrackFactor;
    public FloatReference knockbackX;
    public FloatReference knockbackY;
    public FloatReference damage;

    public Splatter splatterPrefab;
    public Transform splatterPoint;

    private float hp;
    public Transform player;
    private bool tracking;
    private Vector2 bumpVelocity;


	public void Awake () {
        hp = maxHP.Value;
        tracking = false;
	}
	
	protected override void ComputeVelocity () {
        float trackingDist = Mathf.Abs(player.position.x - transform.position.x) + Mathf.Abs(player.position.y - transform.position.y);
        float trackingDir = (player.position.x - transform.position.x) * runSpeed.Value;
        float trackingVel = Mathf.Clamp(trackingDir, -runSpeed.Value, runSpeed.Value);
        Vector2 trackingVelocity = new Vector2(trackingVel, 0);
        Vector2 totalVelocity = Vector2.zero;
        
        if(trackingDist > trackRangeMax.Value * (minTrackFactor.Value + rev.Value))
            tracking = false;
        if(trackingDist < trackRangeMin.Value * (minTrackFactor.Value + rev.Value))
            tracking = true;
        if (tracking)
            totalVelocity += trackingVelocity;
        totalVelocity += bumpVelocity;
        SetVelocity(totalVelocity);
        bumpVelocity = Vector2.zero;
        
	}

    private void SetVelocity(Vector2 vel)
    {
        targetVelocity += vel;
        velocity.y += vel.y;
        if(vel.x > 0)
        {
            facingVector = Vector2.right;
            facingRight = true;
        }
        else
        {
            facingVector = Vector2.left;
            facingRight = false;
        }

    }

    public void DealDamage(float amount, Vector2 facingVector)
    {
        hp -= amount;
        if(hp <= 0)
        {
            hp = 0;
            Die();
        }
        else
        {
            Bump(facingVector);
        }
        Splatter(facingVector);
    }

    public void Die()
    {
        Destroy(this.gameObject);
    }

    public void Bump(Vector2 facingVector)
    {
        Vector2 toAdd = new Vector2(facingVector.x, 1);
        Vector2 mag = new Vector2(knockbackX.Value, knockbackY.Value);
        bumpVelocity = toAdd * mag;
    }

    public void Splatter(Vector2 facingVector)
    {
        
        for (int i = 0; i < 5; i++)
        {
            var splatter = (Splatter)Instantiate(splatterPrefab, splatterPoint.position, splatterPoint.rotation);
            splatter.SetFacing(facingVector);
            splatter.GetComponent<Rigidbody2D>().velocity = Random.Range(4, 7) * facingVector + Random.Range(2, 5) * Vector2.up;

        }
    }
}
