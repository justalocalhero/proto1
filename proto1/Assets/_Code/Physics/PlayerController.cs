﻿using UnityEngine;

public class PlayerController : PhysicsObject {
    public FloatReference jumpSpeed;
    public FloatReference runSpeed;
    public FloatReference shootingRunSlow;
    public FloatReference rev;
    public FloatReference revTime;
    public FloatReference revUpSpeed;
    public FloatReference revDownSpeed;
    public FloatReference revPushback;
    public FloatReference revSpray;
    public FloatReference revFireRate;
    public FloatReference minTurnValue;
    public FloatReference minVelocity;
    public FloatReference maxVelocity;
    public FloatReference maxHP;
    public FloatReference FlashDuration;

    public Transform shootPoint;
    public Transform shellPoint;
    public Bullet bulletPrefab;
    public Shell shellPrefab;
    public Animator MinigunAnimator;
    public SpriteRenderer MuzzleFlash;

    private float hp;
    private float FlashTime;
    private float fireTime;
    private float currentRevTime;
    private bool shooting;

	// Use this for initialization
	void Awake () {
        shooting = false;
        currentRevTime = 0f;
        hp = maxHP.Value;
	}

    public void UpdateTimers()
    {
        fireTime += Time.deltaTime;
        if (FlashTime > 0)
            FlashTime -= Time.deltaTime;
    

    }

    protected override void ComputeVelocity()
    {
        UpdateTimers();
        Vector2 move = Vector2.zero;
        float runSlow = 1f;
        Vector2 pushback = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");
        if(Input.GetButtonDown("Jump") && grounded)
        {
            heavy = false;
            velocity.y = jumpSpeed.Value;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if(velocity.y > 0)
            {
                heavy = true;
            }
        }
        if(Input.GetButtonDown("Fire1"))
        {
            shooting = true;
        }
        else if(Input.GetButtonUp("Fire1"))
        {
            shooting = false;
        }
        if (velocity.y <= 0)
        {
            heavy = true;
        }

        if (shooting)
        {
            runSlow = shootingRunSlow.Value;
            currentRevTime += Time.deltaTime * revUpSpeed.Value;

            pushback = -facingVector * rev.Value * revPushback.Value;
            float fireRate = 1 / (rev.Value * revFireRate.Value);
            if (fireRate == 0)
            {
                Fire();
            }
            else if(fireTime > fireRate)
            {
                Fire();
            }
        }
        else
        {
            currentRevTime -= Time.deltaTime * revDownSpeed.Value;
            if (move.x > minTurnValue.Value)
            {
                SetFacing(true);
      
            }
            if (-move.x > minTurnValue.Value)
            {
                SetFacing(false);
    
            }
        }
        currentRevTime = Mathf.Clamp(currentRevTime, 0, revTime.Value);
        rev.SetValue(EasingFunction.EaseInOutQuad(0, 1, currentRevTime / revTime.Value));
        targetVelocity = move * runSlow * runSpeed.Value + pushback;
        targetVelocity.x = Mathf.Abs(targetVelocity.x) >= minVelocity.Value ? targetVelocity.x : 0;
        targetVelocity.x = Mathf.Clamp(targetVelocity.x, -maxVelocity.Value, maxVelocity.Value);
        if (grounded)
        {
            animator.Play("DudeWalk");
            animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / runSpeed.Value);
        }
        else
            animator.Play("DudeJump");
        MinigunAnimator.SetFloat("rev", rev.Value);
        if (FlashTime > 0)
            MuzzleFlash.enabled = true;
        else
            MuzzleFlash.enabled = false;
    }

    public void Fire()
    {
        fireTime = 0f;
        int shotCount = 1;
        if (rev.Value > .5) shotCount++;
        if (rev.Value == 1) shotCount++;
        for (var i = 0; i < shotCount; i++)
        {
            float sprayMag = revSpray.Value * rev.Value;
            Vector2 sprayComponent = new Vector2(0, Random.Range(-sprayMag, sprayMag) + Random.Range(-sprayMag, sprayMag));
            Vector3 posComponent = new Vector3(Random.Range(0, 2 * sprayMag), Random.Range(-2 * sprayMag, 2 * sprayMag), 0);
            var bullet = (Bullet)Instantiate(bulletPrefab, shootPoint.position + posComponent, shootPoint.rotation);
            bullet.SetFacing(facingVector + sprayComponent);

            if (i == 0)
                bullet.Gunshot.Play();

            var shell = (Shell)Instantiate(shellPrefab, shellPoint.position, shellPoint.rotation);
            shell.SetFacing(facingVector);
            shell.transform.Rotate(new Vector3(0, 0, Random.Range(-20, 20)));
            shell.GetComponent<Rigidbody2D>().velocity = Random.Range(-7, -5) * facingVector + Random.Range(4, 7) * Vector2.up;
        }
        FlashTime = FlashDuration.Value;
    }
}
