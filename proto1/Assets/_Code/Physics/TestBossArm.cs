﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBossArm : MonoBehaviour, IHittable, IFollower {
    private PathFollower engine;

    public void Start()
    {
        engine = GetComponent<PathFollower>();
        engine.StartMoving();
    }

    public void PathComplete()
    {
        engine.StartMoving(0);
    }

    public void Hit(float damage, Vector2 facing)
    {
        
    }
}
