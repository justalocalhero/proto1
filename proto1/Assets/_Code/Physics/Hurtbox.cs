﻿using UnityEngine;

public class Hurtbox : MonoBehaviour {
    public IHittable parent;

    public void Start()
    {
        parent = gameObject.GetComponent<IHittable>();
    }

    public void Hit(float damage, Vector2 facing)
    {
        if (parent != null)
        {
            parent.Hit(damage, facing);
        }
        else
            Debug.Log("No Parent to Hitbox");
    }
}
