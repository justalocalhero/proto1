﻿public interface ITracking : IGroupable {

    void StartTracking();
}
