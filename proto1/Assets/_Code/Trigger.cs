﻿using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour {
    private List<TriggerableObject> triggeredObjects = new List<TriggerableObject>();

    public void Fire()
    {
        for(var i = triggeredObjects.Count - 1; i >= 0; i--)
        {
            triggeredObjects[i].Trigger();
        }
    }

    public void Register(TriggerableObject target)
    {
        if (!triggeredObjects.Contains(target))
            triggeredObjects.Add(target);
    }

    public void UnRegister(TriggerableObject target)
    {
        if (triggeredObjects.Contains(target))
            triggeredObjects.Remove(target);
    }

}
