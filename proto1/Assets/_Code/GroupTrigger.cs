﻿using UnityEngine;

public class GroupTrigger : Trigger {
    public FloatReference maxTimer;
    public FloatReference maxDistance;
    public FloatReference minDistance;

    public Transform target;
    public EnemyGroup group;

    private float timer;
    // Use this for initialization
    void Start () {
        timer = maxTimer.Value;
	}
	
	// Update is called once per frame
	void Update () {
        if (target != null && group != null)
        {
            float distance = BrooklynDistance(transform.position, target.position);
            bool inRange = distance > minDistance.Value && distance < maxDistance.Value;
            bool atCap = group.AtCap();
            bool timeLeft = timer > 0;
            if (inRange && !atCap)
            {
                if (timeLeft)
                { 
                    timer -= Time.deltaTime;
                }
                else
                {
                    timer = maxTimer.Value;
                    Fire();
                }
            }
        }
    }

    public float BrooklynDistance(Vector3 first, Vector3 second)
    {
        return Mathf.Abs(first.x - second.x) + Mathf.Abs(first.y - second.y);
    }
}
