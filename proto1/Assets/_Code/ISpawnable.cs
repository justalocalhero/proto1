﻿using UnityEngine;

public interface ISpawnable {

    ISpawnable Spawn();
    void SetPosition(Vector3 newPosition);
}
