﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FloatReference
{
    public bool UseConstant = false;
    public float ConstantValue;
    public FloatVariable Variable;

    public float Value
    {
        get {
            return UseConstant ? ConstantValue :
              Variable.Value;
        }
    }

    public void ApplyChange(float value)
    {
        Variable.Value += value;
    }

    public void SetValue(float value)
    {
        Variable.Value = value;
    }
}
