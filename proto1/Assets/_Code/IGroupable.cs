﻿using UnityEngine;

public interface IGroupable {
    void SetGroup(EnemyGroup group);
    void SetTarget(Transform target);
    void OnEnable();
    void OnDisable();
}
