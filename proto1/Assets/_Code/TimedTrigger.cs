﻿
using UnityEngine;

public class TimedTrigger : Trigger {
    public FloatReference MaxTriggerTime;

    private float triggerTime;

	void Update () {
        if (triggerTime <= 0)
        {
            triggerTime = MaxTriggerTime.Value;
            Fire();
        }
        else
            triggerTime -= Time.deltaTime;
	}

}
