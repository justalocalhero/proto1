﻿using UnityEngine;

public class ProximityTimer : Trigger {
    public FloatReference maxTimer;
    public FloatReference maxDistance;
    public FloatReference minDistance;

    public Transform target;
    private float timer;
    // Update is called once per frame
    private void Start()
    {
        timer = maxTimer.Value;
    }

    void Update () {
		if(target != null)
        {
            var distance = BrooklynDistance(transform.position, target.position);
            if(distance > minDistance.Value && distance < maxDistance.Value)
            {
                if (timer > 0)
                    timer -= Time.deltaTime;
                else
                {
                    timer = maxTimer.Value;
                    Fire();
                }
            }
        }
	}

    public float BrooklynDistance(Vector3 first, Vector3 second)
    {
        return Mathf.Abs(first.x - second.x) + Mathf.Abs(first.y - second.y);
    }
}
