﻿using UnityEngine;

public class TriggerableObject: MonoBehaviour  {

    public Trigger trigger;

    public virtual void Trigger()
    {
       
    }

    public void OnEnable()
    {
        trigger.Register(this);
    }

    public void OnDisable()
    {
        trigger.UnRegister(this);
    }
}
