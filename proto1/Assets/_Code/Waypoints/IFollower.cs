﻿using UnityEngine;

public interface IFollower
{

    void PathComplete();
    
}
