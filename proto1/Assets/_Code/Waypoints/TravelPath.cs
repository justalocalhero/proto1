﻿using UnityEngine;

public class TravelPath : MonoBehaviour {
    public FloatReference cooldownTime;

    private TravelNode[] travelNodes;
    private int currentNode;
    

    public void Start()
    {
        currentNode = 0;
        travelNodes = GetComponentsInChildren<TravelNode>();
    }

    public Vector3 GetPosition()
    {
        return travelNodes[currentNode].GetPosition();
    }

    public float GetSpeed()
    {
        return travelNodes[currentNode].GetSpeed();
    }

    public float GetWaitTIme()
    {
        return travelNodes[currentNode].GetWaitTIme();
    }

    public bool NextNode()
    {
        bool atFinalNode;

        if (currentNode >= travelNodes.Length - 1)
        {
            currentNode = 0;
            atFinalNode = true;
        }
        else
        {
            currentNode++;
            atFinalNode = false;
        }

        return atFinalNode;
    }

    public void ResetPath()
    {
        currentNode = 0;
    }

    public float GetCooldownTime()
    {
        return cooldownTime.Value;
    }
}
