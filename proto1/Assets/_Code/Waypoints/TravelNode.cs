﻿using UnityEngine;

public class TravelNode : MonoBehaviour {
    public FloatReference speed;
    public FloatReference waitTime;

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public float GetSpeed()
    {
        return speed.Value;
    }

    public float GetWaitTIme()
    {
        return waitTime.Value;
    }
}
