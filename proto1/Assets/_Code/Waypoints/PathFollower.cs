﻿using UnityEngine;

public class PathFollower : MonoBehaviour {
    public FloatReference NodeReachedDistance;

    private TravelPath[] paths;
    private int currentPath;
    private IFollower agent;
    private bool moving;
    private float waitTime;

    public void Start()
    {
        waitTime = 0;
        currentPath = 0;
        moving = false;
        paths = GetComponentsInChildren<TravelPath>();
        agent = GetComponent<IFollower>();

    }

    public void Update()
    {
        if (moving)
        {
            if (waitTime > 0)
                waitTime -= Time.deltaTime;
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, GetTargetPosition(), GetTargetSpeed());
                if(Vector3.Distance(transform.position, GetTargetPosition()) < NodeReachedDistance.Value)
                {
                    bool pathComplete = paths[currentPath].NextNode();
                    waitTime = GeTargettWaitTime();
                    if (pathComplete)
                        agent.PathComplete();
                }
                    
            }
        }
    }

    public Vector3 GetTargetPosition()
    {
        return paths[currentPath].GetPosition();
    }

    public float GetTargetSpeed()
    {
        return paths[currentPath].GetSpeed();
    }

    public float GeTargettWaitTime()
    {
        return paths[currentPath].GetWaitTIme();
    }

    public void StartMoving()
    {
        moving = true;
    }

    public void StartMoving(int path)
    {
        if(path >= 0 && path < paths.Length)
        {
            currentPath = path;
            moving = true;
        }
    }

    public void StopMoving()
    {
        moving = false;
        if(paths[currentPath] != null)
        {
            paths[currentPath].ResetPath();
        }
    }

    public bool GetMoving()
    {
        return moving;
    }

}
