﻿using UnityEngine;
using UnityEditor;

public class CameraController : MonoBehaviour {
    private Vector2 velocity;
    public Transform target;

    public FloatReference smoothingUp;
    public FloatReference smoothingDown;
    public FloatReference smoothingX;
    public FloatReference offsetX;
    public FloatReference offsetY;

    // Update is called once per framea
    public void FixedUpdate () {
        float smoothY = target.position.y + offsetY.Value> transform.position.y ? smoothingUp.Value : smoothingDown.Value;
        float posX = Mathf.Lerp(transform.position.x, target.position.x + offsetX.Value, smoothingX.Value);
        float posY = Mathf.Lerp(transform.position.y, target.position.y + offsetY.Value, smoothY);

        transform.position = new Vector3(posX, posY, transform.position.z);
	}
}
