﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingGroup : EnemyGroup {
    public Transform target;

    protected override void OnRegister(EnemyController member)
    {
        member.SetTarget(target);
    }

    protected override void OnUnRegister(EnemyController member)
    {
        
    }

    public void StartTracking()
    {
        for (var i = 0; i < members.Count; i++)
        {
            members[i].StartTracking();
        }
    }
}
