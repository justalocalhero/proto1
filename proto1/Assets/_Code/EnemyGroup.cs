﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyGroup : MonoBehaviour {
    public FloatReference cap;
    public List<EnemyController> members = new List<EnemyController>();

    public void RegisterMember(EnemyController member)
    {
        if(!members.Contains(member))
        {
            members.Add(member);
            OnRegister(member);
        }
        
    }

    public void UnRegisterMember(EnemyController member)
    {
        if(members.Contains(member))
        {
            members.Remove(member);
            OnUnRegister(member);
        }
    }

    protected virtual void OnRegister(EnemyController member)
    {

    }

    protected virtual void OnUnRegister(EnemyController member)
    {

    }

    public bool AtCap()
    {
        return members.Count >= cap.Value;
    }
}
