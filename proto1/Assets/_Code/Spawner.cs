﻿using UnityEngine;

public class Spawner : TriggerableObject {
    public EnemyController toSpawn;
    public Transform spawnPoint;
    public Transform player;
    public EnemyGroup group;

    public override void Trigger()
    {
        var newSpawn = toSpawn.Spawn();
        newSpawn.SetPosition(spawnPoint.position);
        if (player != null)
        {
            newSpawn.SetTarget(player);
        }
        if (group != null)
        {
            newSpawn.SetGroup(group);
        }

    }
}
